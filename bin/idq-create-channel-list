#!/usr/bin/env python

import argparse
import os

from gwpy.detector import ChannelList


# define CLI
parser = argparse.ArgumentParser()
parser.add_argument(
    "channel_list",
    metavar="CHANNEL-LIST",
    help="the channel list to use to determine safety information from",
)
parser.add_argument(
    "-o",
    "--output",
    default="safe-channels.txt",
    help="the filename to write the safe channel list to",
)

args = parser.parse_args()

# read and filter channels by safety information
safe_channels = [
    channel.name for channel in ChannelList.read(args.channel_list) if channel.safe
]

# write safe channels to disk
with open(args.output, "w") as f:
    f.write(os.linesep.join(safe_channels))
