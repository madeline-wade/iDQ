#!/usr/bin/env python

import argparse

from idq import batch
from idq import cli


# command line options
parser = argparse.ArgumentParser()

cli.add_logging_options(parser)
cli.add_batch_options(parser)

parser.add_argument(
    "--skip-timeseries",
    action="store_true",
    help="skip reading in timeseries and all related plots",
)
parser.add_argument(
    "--reportdir",
    type=str,
    help="if set, point at an alternate directory to create a report",
)

# parse and validate
args = parser.parse_args()
cli.validate_times(args)

# run
cli.set_up_logger(args)
batch.report(args.start, args.end, args.config, **cli.format_args(args))
