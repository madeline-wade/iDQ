import pytest


@pytest.mark.usefixtures("stream_processor")
class TestStreamProcessor(object):
    """
    Tests several aspects of StreamProcessor.
    """

    def test_timestamp(self, kafka_dataloader_conf):
        stream = self.stream_processor
        while len(stream) == 0:
            data, segs = stream.poll()
        assert (
            data.segs in segs.coalesce()
        ), "span of data in DataChunk is inconsistent with timestamps"
        stream.flush()

    def test_poll(self, kafka_dataloader_conf):
        stream = self.stream_processor
        while len(stream) == 0:
            data, _ = stream.poll()

        assert set(data.channels) == set(
            kafka_dataloader_conf["channels"]
        ), "incorrect channels stored in DataChunk after polling"
        stream.flush()

    def test_flush(self, kafka_dataloader_conf, retain=1):
        stream = self.stream_processor
        # get at least 2 buffers, store timestamp for first
        while len(stream) == 0:
            timestamp = stream.timestamp
            _, segs = stream.poll()
        while len(stream) < 2:
            stream.poll()

        timestamp = segs[0][0]

        stream.flush(retain=retain)
        assert (
            len(stream._queue) == retain
        ), "should only be one DataLoader object after flushing stream"
        assert (
            stream._queue[0].start >= timestamp
        ), "buffer should have start time at least as large as the current timestamp"
        assert (
            stream._buffer.start >= timestamp
        ), "buffer stored in StreamProcessor should have a start time >= timestamp"
        stream.flush()
