
iDQ
==========================================================================

[![pipeline status](https://git.ligo.org/lscsoft/iDQ/badges/main/pipeline.svg)](https://git.ligo.org/lscsoft/iDQ/commits/main)
[![coverage](https://git.ligo.org/lscsoft/iDQ/badges/main/coverage.svg)](https://git.ligo.org/lscsoft/iDQ/commits/main)
[![conda-forge](https://img.shields.io/conda/vn/conda-forge/idq.svg)](https://anaconda.org/conda-forge/idq)

|              |        |
| ------------ | ------ |
| **Version:** | 0.6.2  |
| **Docs:**     | https://lscsoft.docs.ligo.org/iDQ  |
| **Source:**  | http://software.igwn.org/lscsoft/source/iDQ-0.6.2.tar.gz  |

iDQ, or inferential Data Quality, is a statistical inference framework for data quality.
It specifically focuses on the problem of non-Gaussian noise transients within
gravitational-wave detectors, but the underlying formalism has broader applicability.

iDQ works with vectorized representations of the detector’s auxiliary state and searches for correlations
between that vectorized state and noise transients in h(t) with the end goal of producing a calibrated
estimate of the probability that there is a noise artifact in h(t), conditioned on the auxiliary state,
as a function of time. This is primarily done through supervised learning, and iDQ supports a variety of
supervised learning techniques. Furthermore, these concepts extend well beyond 1-dimensional data
(i.e.: timeseries) and could be applied to any streaming classification problem.

iDQ not only supports classification through 2-class classification schemes, but also supports automatic
retraining and calibration to deal with detector non-stationarity. In this way, the algorithm automatically
re-learns which correlations are important as they change over time and returns meaningful probabilistic
statements that can be interpreted immediately without further processing.

## Installation/Quickstart

  * [Installation](https://docs.ligo.org/lscsoft/iDQ/installation.html)
  * [Running Batch Jobs](https://docs.ligo.org/lscsoft/iDQ/tutorials/running_batch_pipeline.html)
  * [Running Streaming Jobs](https://docs.ligo.org/lscsoft/iDQ/tutorials/running_stream_pipeline.html)

## References

  * [iDQ: Statistical Inference of Non-Gaussian Noise with Auxiliary Degrees of Freedom in Gravitational-Wave Detectors](https://arxiv.org/abs/2005.12761)
